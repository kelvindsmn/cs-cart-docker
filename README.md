# CS Cart Docker

Easy way to running CS Cart locally or production using Docker :)

### Prerequisites
You need to install this:

* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)


### Installing

* Git clone this repository
* Unzip cs-cart source code 'multivendor_v4.11.3.zip' to /src directory
* Modify .env according to your credentials
* Modify according to your OS /etc/host follow this [instruction](https://www.howtogeek.com/howto/27350/beginner-geek-how-to-edit-your-hosts-file/)
* add this to /etc/host ```127.0.0.1 php72.cs-cart.local```
* Continue cs-cart installation

```
docker-compose up -d --build
```

* open http://localhost:8888/

## Technology

* [CS Cart](https://www.cs-cart.com/) - ECOMMERCE Platform
* [PHP7](https://www.php.net/releases/7_0_0.php) - PHP 7
* [MySQL](https://www.mysql.com/) - MySQL 5 Database
* [Nginx](https://httpd.apache.org/) - Apache Web Server

## Authors

* **Kelvin** 
